import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiectul2 {

    // sub 2, aplicatie Java

    public class ScriereFisier {
        private JPanel panel;
        private JTextArea textArea1;
        private JButton button1;

        public ScriereFisier() {
            button1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        FileWriter myWriter = new FileWriter("text.txt");
                        myWriter.write(textArea1.getText());
                        myWriter.close();
                        System.out.println("Successfully wrote to the file.");
                    } catch (IOException et) {
                        System.out.println("An error occurred.");
                        et.printStackTrace();
                    }
                }
            });

        }

        public static void main(String[] args) {
            JFrame frame = new JFrame("Afisare in fisier");
            frame.setContentPane(new ScriereFisier().panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
            frame.setSize(250, 250);

        }
    }
}
